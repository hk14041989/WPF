﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF02
{
    public class CustomGridPanel:Grid
    {
        private enum Action
        {
            Open,
            Close
        }
        private const double DEFAULT_SPEED = 5;
        private System.Windows.Threading.DispatcherTimer dispatcherTimer;
        private double speed;
        private double exspextedMinWidth;
        private double exspextedMaxWidth;
        public static event PropertyChangedCallback IsOpenChanged;
        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty,value); }
        }
        public static readonly DependencyProperty IsOpenProperty = DependencyProperty.Register("IsOpen", typeof(bool), typeof(CustomGridPanel)
            ,new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.AffectsRender, IsOpenChangedAction));
        private static void IsOpenChangedAction(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (IsOpenChanged != null)
            {
                IsOpenChanged.Invoke(d, e);
            }
        }
        public CustomGridPanel()
        {
            IsOpenChanged += (sender, e) =>
            {
                InvokeAction(IsOpen ? Action.Open : Action.Close);
            };

        }
        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            exspextedMaxWidth = Width;
            exspextedMinWidth = MinWidth;
        }
        private void InvokeAction(Action action)
        {
            if (IsLoaded)
            {
                dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 3);
                speed = DEFAULT_SPEED;
                if (action == Action.Open)
                {
                    OpenAnimation();
                }
                else 
                {
                    CloseAnimation();
                }
                dispatcherTimer.Start();
            }
            else if(action==Action.Close)
            {
                Visibility = Visibility.Collapsed;
                Width = exspextedMinWidth;
            }
            else if(action ==Action.Open)
            {
                Visibility = Visibility.Visible;
                Width = exspextedMaxWidth;

            }
        }
        private void CloseAnimation()
        {
            dispatcherTimer.Tick += (sender, e) =>
            {
                if (Width > exspextedMinWidth)
                {
                    speed += 1;

                    if (Width - exspextedMinWidth < speed)
                    {
                        speed = Width - exspextedMinWidth;
                    }

                    Width -= speed;
                }
                else
                {
                    // Stop timer
                    dispatcherTimer.Stop();
                    Visibility = Visibility.Collapsed;
                }
            };
        }
        private void OpenAnimation()
        {
            Visibility = Visibility.Visible;

            dispatcherTimer.Tick += (sender, e) =>
            {
                if (Width < exspextedMaxWidth)
                {
                    speed += 1;

                    if (exspextedMaxWidth - Width < speed)
                    {
                        speed = exspextedMaxWidth - Width;
                    }

                    Width += speed;
                }
                else
                {
                    // Stop timer
                    dispatcherTimer.Stop();
                }
            };
        }
    }
   
}
